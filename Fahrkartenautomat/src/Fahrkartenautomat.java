﻿import java.util.Scanner;

class Fahrkartenautomat{

	public static void pr(String line) {
		System.out.print(line);
	}
	
	public static void prline(String line) {
		System.out.println(line);
	}
	
	public static void warte(int zeitZuWarten) {
		try {
		Thread.sleep(zeitZuWarten);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return;
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag+ " " + einheit);
		return;
	}
	
	public static double fahrkartenbestellungErfassen(){
		double zuZahlenderBetrag = 0;
		byte art_ticket = 0;
		byte anzahl_tickets = 0; // 
	    
		//Welche Vorteile hat man durch diesen Schritt?
		//Vorteil wäre verschiedene Arrays für unterschiedlich Sprachen,
		//einfacher zu ergänzen.
	    String[] fahrkartenTypen = {
	    		"Einzelfahrschein Berlin AB",
	    		"Einzelfahrschein Berlin BC",
	    		"Einzelfahrschein Berlin ABC",
	    		"Kurzstrecke",
	    		"Tageskarte Berlin AB",
	    		"Tageskarte Berlin BC",
	    		"Tageskarte Berlin ABC",
	    		"Kleingruppen-Tageskarte Berlin AB",
	    		"Kleingruppen-Tageskarte Berlin BC",
	    		"Kleingruppen-Tageskarte Berlin ABC"
	    };
	    
	    double[] preise = {
	    		2.90,
	    		3.30,
	    		3.60,
	    		1.90,
	    		8.60,
	    		9.00,
	    		9.60,
	    		23.50,
	    		24.30,
	    		24.90
	    };
	    
	    
	    Scanner tastatur = new Scanner(System.in);
	    
	    prline("Fahrkartenbestellvorgang:");
	    prline("=========================");
	    prline("");
	    prline("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
	    for (int index = 0; index < fahrkartenTypen.length; index++) {
	    	System.out.printf("(%2d) %-36s%.2f\n",index + 1, fahrkartenTypen[index],preise[index] );
	    }
	    
	    prline("");
	    
	    boolean valid_ticket = false;
	    
	    while (!valid_ticket) {
	    	pr("Ihre Wahl: ");
	    	art_ticket = tastatur.nextByte(); 
	    	art_ticket--;
	    	for (int index = 0; index < fahrkartenTypen.length; index++ ) {
	    		if (art_ticket == index) {
	    			valid_ticket = true;
	    		}
	    	}
	    	if (!valid_ticket) {
	    		prline("  >>falsche Eingabe<<");
	    	}
	    }
	    
	    System.out.printf("Ticketpreis (Euro) : %.2f\n", preise[art_ticket]);
	    
	    while (!(anzahl_tickets >= 1 && anzahl_tickets <= 10)) {
	    	pr("Anzahl der Tickets : ");
	    	anzahl_tickets = tastatur.nextByte();
	    	if (anzahl_tickets < 1 || anzahl_tickets > 10) {
	    		prline("  >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
	    		prline("");
	    	}
	    }
	       
	    System.out.printf("Zu zahlender Betrag(EURO): ");
	    
	    zuZahlenderBetrag = preise[art_ticket] * anzahl_tickets;
	     
	    art_ticket = 0;
		
	    return zuZahlenderBetrag;
	}
	public static double fahrkartenBezahlen(double zuZahlen){
		double rückgeld = 0;
		
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag = 0.0;
		
	       while(eingezahlterGesamtbetrag < zuZahlen)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlen - eingezahlterGesamtbetrag));
	    	   System.out.printf("Eingabe (mind. 5Ct, höchstens 2 Euro):  ");
	    	   double eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       
	       rückgeld = eingezahlterGesamtbetrag - zuZahlen;
		
		return rückgeld;
	}
	public static void fahrkartenAusgeben(){
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          warte(250);
	       }
	       System.out.println("\n\n");
	    return;
	}
	public static void rueckgeldAusgeben(double rückgabebetrag){		
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%,.2f",rückgabebetrag) + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	   muenzeAusgeben(2,"EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	   muenzeAusgeben(1,"EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	   muenzeAusgeben(50,"CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	   muenzeAusgeben(20,"CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	   muenzeAusgeben(10,"CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(5,"CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	       
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir wünschen Ihnen eine gute Fahrt.");
		
		return;
	}
	
	
    public static void main(String[] args)
    {
       double zuZahlenderBetrag;
       // double eingezahlterGesamtbetrag; // deklariert die Gleitkommazahl
       // double eingeworfeneMünze; // deklariert  die Gleitkommazahl
       double rückgabebetrag; // deklariert die Gleitkommazahl
       
       while(true) {
    	   
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       

       // Geldeinwurf
       // -----------
       
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       // Fahrscheinausgabe
       // -----------------
       
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rueckgeldAusgeben(rückgabebetrag);
       }
    }
}