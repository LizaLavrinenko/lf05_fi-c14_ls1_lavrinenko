import java.util.ArrayList;

public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemInProzent;
    private String schiffsname;
    private int androidenAnzahl;
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
    static private ArrayList<String> broadcastKommunikation = new ArrayList<String>();

    //Setter
    public void setphotonentorpedoAnzahl(int new_photonentorpedoAnzahl)
    {
    	if (isInProzent(new_photonentorpedoAnzahl))
        this.photonentorpedoAnzahl = new_photonentorpedoAnzahl;
    }

    public void setenergieversorgungInProzent(int new_energieversorgungInProzent)
    {
    	if (isInProzent(new_energieversorgungInProzent))
        this.energieversorgungInProzent = new_energieversorgungInProzent;
    }

    public void setschildInProzent(int new_schildInProzent)
    {
    	if (isInProzent(new_schildInProzent))
        this.schildInProzent = new_schildInProzent;
    }

    public void sethuelleInProzent(int new_huelleInProzent)
    {
    	if (isInProzent(new_huelleInProzent))
        this.huelleInProzent = new_huelleInProzent;
    }

    public void setlebenserhaltungssystemInProzent(int new_lebenserhaltungssystemInProzent)
    {
    	if (isInProzent(new_lebenserhaltungssystemInProzent))
        this.lebenserhaltungssystemInProzent = new_lebenserhaltungssystemInProzent;
    }

    public void setschiffsname(String new_schiffsname)
    {
        this.schiffsname = new_schiffsname;
    }

    public void setandroidenAnzahl(int new_androidAnzahl)
    {
        this.androidenAnzahl = new_androidAnzahl;
    }

    //Getter
    public int getphotonentorpedoAnzahl()
    {
        return this.photonentorpedoAnzahl;
    }

    public int getenergieversorgungInProzent()
    {
        return this.energieversorgungInProzent;
    }

    public int getschildInProzent()
    {
        return this.schildInProzent;
    }

    public int gethuelleInProzent()
    {
        return this.huelleInProzent;
    }

    public int getlebenserhaltungssystemInProzent()
    {
        return this.lebenserhaltungssystemInProzent;
    }

    public String getschiffsname()
    {
        return this.schiffsname;
    }

    public int getandroidenAnzahl()
    {
        return this.androidenAnzahl;
    }

    //addLadung

    public void addLadung(Ladung new_Ladung)
    {
        this.ladungsverzeichnis.add(new_Ladung);
    }
    
    //returnLadungsverzeichnis
    
    public ArrayList<Ladung> returnLadungsverzeichnis()
    {
    	return this.ladungsverzeichnis;
    }
    
    //4.2 Methoden
    
    private void treffer_vermerken() {
    	System.out.println(this.schiffsname + " wurde getroffen!");
    }
    
    public void torpedos_abschiessen(Raumschiff ziel) {
    	if (0 == this.photonentorpedoAnzahl) {
    		this.nachricht_an_Alle("-=*Click*=-");
    	}
    	else {
    		this.photonentorpedoAnzahl -=1;
    		ziel.treffer_vermerken();
    	}
    }
    public void phaserkanonen_abschiessen(Raumschiff ziel) {
    	if (50 > this.photonentorpedoAnzahl) {
    		this.nachricht_an_Alle("-=*Click*=-");
    	}
    	else {
    		this.energieversorgungInProzent -=50;
    		this.nachricht_an_Alle("Phaserkanone abgeschossen");
    		ziel.treffer_vermerken();
    	}
    	
    }
    public void nachricht_an_Alle(String nachricht) {
    	System.out.println(this.schiffsname + ": " + nachricht);
    }
    public void ausgabe_logbuch() {
    	
    }
    public void if_photoTorpedo_load(int anzahl) {
    	
    }
    public void reperaturauftrag_losschicken(boolean schild, boolean huelle, boolean energie, int androide) {
    	
    }
    public void ladungsverzeichnis_ausgeben() {
    	System.out.println("Ladungsverzeichnis: <");
    	for (Ladung i :this.ladungsverzeichnis){
    		System.out.println("Art: " + i.getBezeichnung() + " Menge: " + i.getMenge());
    	}
    	System.out.println(">");
    }
    public void zustand_ausgeben() {
    	System.out.println("-------");
    	System.out.println("Schiffsname: " + this.schiffsname);
    	System.out.println("Anzahl Photonentorpedos: "+ this.photonentorpedoAnzahl);
    	System.out.println("Energieversorgung: "+ this.energieversorgungInProzent +"%");
    	System.out.println("Schild: "+ this.schildInProzent +"%");
    	System.out.println("H�lle: "+ this.huelleInProzent +"%");
    	System.out.println("Lebenserhaltungssysteme: "+ this.lebenserhaltungssystemInProzent +"%");
    	System.out.println("Anzahl Androiden: "+ this.androidenAnzahl);
    	//this.ladungsverzeichnis_ausgeben();
    	System.out.println("-------");
    }
    //helper
    private boolean isInProzent(int value) 
    {
    	return (100 <= value && 0 >= value);
    }

    Raumschiff(){
    	this.photonentorpedoAnzahl = 0;
        this.energieversorgungInProzent = 100;
        this.schildInProzent = 100;
        this.huelleInProzent = 100;
        this.lebenserhaltungssystemInProzent = 100;
        this.schiffsname = null;
        this.androidenAnzahl = 0;
    }
    
    Raumschiff(
        int new_photonentorpedoAnzahl,
        int new_energieversorgungInProzent,
        int new_schildInProzent,
        int new_huelleInProzent,
        int new_lebenserhaltungssystemInProzent,
        String new_schiffsname,
        int new_androidAnzahl)
    {
        this.photonentorpedoAnzahl = new_photonentorpedoAnzahl;
        this.energieversorgungInProzent = new_energieversorgungInProzent;
        this.schildInProzent = new_schildInProzent;
        this.huelleInProzent = new_huelleInProzent;
        this.lebenserhaltungssystemInProzent = new_lebenserhaltungssystemInProzent;
        this.schiffsname = new_schiffsname;
        this.androidenAnzahl = new_androidAnzahl;
    }
    
    
}