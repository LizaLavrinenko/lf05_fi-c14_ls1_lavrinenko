import java.util.ArrayList;

public class Main {
    public static void main(String[] args){ 
        //4.1
    	Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
        Raumschiff vulkanier = new Raumschiff(0, 80, 50, 100, 100, "Ni'Var", 5);

        //beladen
        Ladung saft = new Ladung("Ferengi Schneckensaft", 200);
        Ladung schwert = new Ladung("Bat'leth Klingonen Schwert",200);
        klingonen.addLadung(saft);
        klingonen.addLadung(schwert);
        
        //direkte instanzierung im Funktionsaufruf
        romulaner.addLadung(new Ladung("Borg-Schrott",5));
        romulaner.addLadung(new Ladung("Rote Materie",2));
        romulaner.addLadung(new Ladung("Plasma-Waffe",50));
        
        vulkanier.addLadung(new Ladung("Forschungssonde",35));
        vulkanier.addLadung(new Ladung("Photonentorpedo",3));
        
        //4.2
        zustandAusgeben(klingonen);
        zustandAusgeben(romulaner);
        zustandAusgeben(vulkanier);
        
        //4.2.1
        System.out.println("Aufgabe 4.2.1");
        
        klingonen.torpedos_abschiessen(romulaner);
        romulaner.phaserkanonen_abschiessen(klingonen);
        vulkanier.nachricht_an_Alle("Gewalt ist nicht logisch");
        klingonen.zustand_ausgeben();
        klingonen.ladungsverzeichnis_ausgeben();
        klingonen.torpedos_abschiessen(romulaner);
        klingonen.zustand_ausgeben();
        klingonen.ladungsverzeichnis_ausgeben();
        romulaner.zustand_ausgeben();
        romulaner.ladungsverzeichnis_ausgeben();
        vulkanier.zustand_ausgeben();
        vulkanier.ladungsverzeichnis_ausgeben();
    }
    
    public static void zustandAusgeben(Raumschiff raumschiff) 
    {
    	System.out.println("---Status----");
    	System.out.println("Name: " + raumschiff.getschiffsname());
    	System.out.println("Anzahl Photonentorpedo: "+ raumschiff.getphotonentorpedoAnzahl());
    	System.out.println("Energieversorgung: "+ raumschiff.getenergieversorgungInProzent() + "%");
    	System.out.println("Schild: " + raumschiff.getschildInProzent()+"%");
    	System.out.println("Huelle: " + raumschiff.gethuelleInProzent()+"%");
    	System.out.println("Lebenserhaltungssystem" + raumschiff.getlebenserhaltungssystemInProzent()+"%");
    	System.out.println("Anzahl Androiden: " + raumschiff.getandroidenAnzahl());
    	System.out.println("--------------");
    }
    

    
    public static void ladungsverzeichnisAusgeben(Raumschiff raumschiff)
    {
    	ArrayList<Ladung> ladungsverzeichnis = raumschiff.returnLadungsverzeichnis();
    }
    
    //if Photonentorpedo:anzahl 

}