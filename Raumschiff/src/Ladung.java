public class Ladung {
    private String bezeichnung;
    private int menge;

    public String getBezeichnung() 
    {
        return this.bezeichnung;
    }

    public int getMenge() 
    {
        return this.menge;
    }

    public void setBezeichnung(String new_bezeichnung) 
    {
        this.bezeichnung = new_bezeichnung;
    }

    public void setMenge(int new_menge) 
    {
        this.menge = new_menge;
    }

    Ladung(){
    	this.bezeichnung = null;
    	this.menge = 0;
    }
    
    Ladung(String new_bezeichnung, int new_menge)
    {
        this.bezeichnung = new_bezeichnung;
        this.menge = new_menge;
    }
}
