import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		//Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();
		String artikel = liesString("was m�chten Sie bestellen?");
		
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();
		double preis = liesDouble("Geben Sie den Nettopreis ein:");

		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		//double nettogesamtpreis = anzahl * preis;
		//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis,mwst);

		// Ausgeben

		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		//rechungausgaben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst)
		rechungausgaben(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis,mwst);
		
	}
	
	public static String liesString(String text) {
		Scanner myscanner = new Scanner(System.in);
		
		System.out.println(text);
		return myscanner.nextLine();
	}
	public static int liesInt(String text) {
		Scanner myscanner = new Scanner(System.in);
		
		System.out.println(text);
		return myscanner.nextInt();
	}
	public static double liesDouble(String text) {
		Scanner myscanner = new Scanner(System.in);
		
		System.out.println(text);
		return myscanner.nextDouble();
	}
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	public static void rechungausgaben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
