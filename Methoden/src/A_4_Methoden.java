
public class A_4_Methoden {

	public static void main(String[] args) {
		System.out.println(würfel(5));
		System.out.println(quader(5,5,5));
		System.out.println(pyramide(5,5));
		System.out.println(kugel(5));
		

	}
	
	public static double würfel(double parameter1) {
		return parameter1 * parameter1 * parameter1;
	}

	public static double quader(double parameter1, double parameter2, double parameter3) {
		return parameter1 * parameter2 * parameter3;
	}
	
	public static double pyramide(double parameter1, double parameter2) {
		return parameter1 * parameter1 * parameter2 / 3;
	}
	
	public static double kugel(double parameter1) {
		return 4/3 * parameter1 * parameter1 * parameter1 * 3.14;
	}
}


//a) Würfel: V = a * a * a
//b) Quader: V = a * b * c
//c) Pyramide: V = a * a * h / 3
//d) Kugel: V = 4/3 * r³ * π. 