public class Konfiguration_Chaos {
	
	public static void main(String[] args) {
		//Deklaration von Variablen
		String name;
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		name = typ + " " + bezeichnung; 
		// Deklaration von Variablen
		int euro;
		int cent;
		char sprachModul = 'd';
		int summe; 
		double fuellstand; 
		final byte PRUEFNR = 4; 
		//Inizalisierung von Variablen
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		//Berechnung vom Geld
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
		cent = summe % 100;

		// Inizialisierung 
		double maximum = 100.00;
		double patrone = 46.24;
		fuellstand = maximum - patrone;
		boolean statusCheck;
		statusCheck = (euro <= 150) && (sprachModul == 'd') && (fuellstand >= 50.00) && (!(PRUEFNR == 5 || PRUEFNR == 6)) && (euro >= 50)
		&& (cent != 0); 
		
		//Print 
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pruefnummer : " + PRUEFNR);
		System.out.println("Fuellstand Patrone: " + fuellstand + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");           
		System.out.println("Status: " + statusCheck); 
		
	}
}