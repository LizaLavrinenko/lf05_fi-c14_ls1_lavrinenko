import java.util.Scanner; // Import der Klasse Scanner
public class Rechner
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
 int zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
 int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis = zahl1 + zahl2;
 int ergebnis_2 = zahl1 - zahl2;
 int ergebnis_3 = zahl1 * zahl2; 
 
 System.out.print("\nErgebnis der Addition lautet: ");
 System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
 System.out.print("Ergebnis der Subbtraktion lautet: ");
 System.out.println(zahl1 + " - " + zahl2 +" = " + ergebnis_2); 
 System.out.print("Ergebnis der Multiplikation lautet: ");
 System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis_3);
 System.out.print("Ergebnis der Division lautet: ");
 System.out.println(zahl1 + " : " + zahl2 + " = " + ((double)zahl1/zahl2));
 
 
 myScanner.close();

 }
}