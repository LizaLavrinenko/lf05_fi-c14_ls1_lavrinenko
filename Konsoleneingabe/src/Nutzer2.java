import java.util.Scanner;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Nutzer2 {
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Scanner myscanner = new Scanner(System.in);
		Date date = new Date();
		
		String name;
		int jahr;
		int aktuellesJahr;
		
		System.out.print("Hallo! Wie hei�t du?");
		
		name = myscanner.nextLine();
		
		System.out.print("Wie alt bist du?");
		
		jahr = myscanner.nextInt();
		
		ZoneId timeZone = ZoneId.systemDefault();
        LocalDate getLocalDate = date.toInstant().atZone(timeZone).toLocalDate();
        aktuellesJahr = getLocalDate.getYear();

		System.out.print("Du wurdest im Jahr " + (aktuellesJahr-jahr) + " geboren");
	}

}
