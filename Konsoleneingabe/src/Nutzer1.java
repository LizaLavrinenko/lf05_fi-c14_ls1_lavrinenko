import java.util.Scanner;
public class Nutzer1 {

	public static void main(String[] args) {
		
		Scanner dieter = new Scanner(System.in);
		 String name;
		 int alter;
		 
		 System.out.println("Hallo User! Wie ist dein Name?");
		 
		 name = dieter.nextLine();
		 
		 System.out.println("Hallo " + name + ", wie alt bist du?");
		 
		 alter = dieter.nextInt();
		 
		 System.out.print("Dein Name ist " + name + " und du bist " + alter + " Jahre alt");
		 
		 dieter.close();
		 
	}
}
