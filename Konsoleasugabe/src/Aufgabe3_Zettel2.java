
public class Aufgabe3_Zettel2 {

	public static void main(String[] args) {
		System.out.printf("%-12s|", "Fahrenheit");
		System.out.printf("%10s", "Celsius");
		System.out.print("\n------------------------");
		System.out.printf("%n%-12s|","-20");
		System.out.printf("%10.6s\n","-28.8889");
		System.out.printf("%-12s|", "-10");
		System.out.printf("%10.6s\n","-23.3333");
		System.out.printf("%-12s|","+0");
		System.out.printf("%10.6s\n","-17.7778");
		System.out.printf("%-12s|", "+20");
		System.out.printf("%10.5s", "-6.6667");
	}

}
