/*
 Ein Hardware-Gro�h�ndler m�chte den Rechnungsbetrag einer Bestellung f�r PC-M�use mit
einem Programm ermitteln. Wenn der Kunde mindestens 10 M�use bestellt, erfolgt die
Lieferung frei Haus, bei einer geringeren Bestellmenge wird eine feste Lieferpauschale von
10 Euro erhoben. Vom Gro�h�ndler werden die Anzahl der bestellten M�use und der
Einzelpreis eingegeben. Das Programm soll den Rechnungsbetrag (incl. MwSt.) ausgeben.

 */

import java.util.Scanner;

public class Verzwegung_44_3 {
	
	public static double preis_maus() {
		Scanner myscanner = new Scanner (System.in);
		
		System.out.println("Wieviel kostet eine Maus?");
		
		return myscanner.nextDouble();
	}
	
	public static int menge(){
		Scanner myscanner = new Scanner(System.in);
		
		System.out.println("Wieviel M�use bestellen?");
		
		int menge_m�use = myscanner.nextInt();
		
		return menge_m�use;
	}
	
	public static double rechnung(int anzahl_m�use_2, double einzelpreis_2) {
		double result;
		
		if (anzahl_m�use_2 >= 10)
		{
			result = anzahl_m�use_2 * einzelpreis_2;
		}
		else {
		result = ((anzahl_m�use_2 * einzelpreis_2) + 10);
		}
		return result;
	}
	
	public static void ausgabe(double gesamt_preis_2) {
		
		System.out.println("Die Summe betr�gt: " + gesamt_preis_2);
		
	}
	
	public static void main(String[] args) {

	double einzelpreis = preis_maus();
		
		int anzahl_m�use = menge();
	
		double gesamt_preis = rechnung(anzahl_m�use,einzelpreis);
		
		ausgabe(gesamt_preis);
		

	}

}
