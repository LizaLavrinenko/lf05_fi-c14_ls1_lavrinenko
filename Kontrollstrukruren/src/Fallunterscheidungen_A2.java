import java.util.Scanner;
public class Fallunterscheidungen_A2 {
	
	public static int ziffer_Eingeben() {
		
		Scanner myscanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl zwischen 1 und 12 ein: ");
		
		return myscanner.nextInt();	
	}
	
	public static void monat_ausgeben(String monat) {
		System.out.println(monat);	
	}

	public static void main(String[] args) {
	
		/*Der Benutzer gibt eine Ziffer zwischen 1 und 12 ein. Ihr Java-Programm ermittelt den
			entsprechenden Monat.*/ 
		
		int zahl = ziffer_Eingeben();
		
		switch (zahl) {
			case 1:
				monat_ausgeben("Januar");
				break;
			case 2:
				monat_ausgeben("Februar");
				break;
			case 3:
				monat_ausgeben("Maerz");
				break;
			case 4:
				monat_ausgeben("April");
			case 5:
				monat_ausgeben("Mai");
			case 6:
				monat_ausgeben("Juni");
			case 7:
				monat_ausgeben("Juli");
			case 8:
				monat_ausgeben("August");
			case 9:
				monat_ausgeben("September");
			case 10:
				monat_ausgeben("Oktober");
			case 11:
				monat_ausgeben("November");
			case 12:
				monat_ausgeben("Dezember");
			default:
				System.out.println("Geben Sie eine Zahl zwischen 1 und 12 ein.");
				break;
		}
		}

}
