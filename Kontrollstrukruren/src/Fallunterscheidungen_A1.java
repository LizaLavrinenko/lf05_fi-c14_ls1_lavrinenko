import java.util.Scanner;

public class Fallunterscheidungen_A1 {

	public static void main(String[] args) {
		 /* Erstellen Sie die Konsolenanwendung Noten. Das Programm Noten soll nach der Eingabe
			einer Ziffer die sprachliche Umschreibung ausgeben (1 = Sehr gut, 2 = Gut, 3 = Befriedigend,
			4 = Ausreichend, 5 = Mangelhaft, 6 = Ungenügend). Falls eine andere Ziffer eingegeben
			wird, soll ein entsprechender Fehlerhinweis ausgegeben werden. 
		  */
		Scanner myscanner = new Scanner(System.in);
		
		int note;
		
		System.out.println("Geben Sie die Note ein: ");
		
		note = myscanner.nextInt();
		
		if (note == 1) {
			System.out.println("Sehr gut");
		} else if (note == 2) {
			System.out.println("Gut");
		} else if (note == 3) {
			System.out.println("Befriedigend");
		} else if (note == 4) {
			System.out.println("Ausreichend");
		} else if (note == 5) {
			System.out.println("Mangelhaft");
		} else  if (note == 6) {
			System.out.println("Ungenügend");
		} else {
			System.out.println("Geben Sie eine Zahl zwischen 1-6");
		}
	}

}
