import java.util.Scanner;
public class Zaehlen {

	public static void main(String[] args) {
		/*Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend (bzw. von n bis 1
		herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen. Nutzen Sie
		zur Umsetzung eine for-Schleife.*/
		
		Scanner myscanner = new Scanner(System.in);
		
		System.out.print("Gib eine Zahl ein: ");
		int n = myscanner.nextInt();
		
		for(int x =1; x<=n; x++) {
			System.out.print(x + ",");
			
		}
		System.out.println();
		
		for(int x=n; x>=1;x--) {
			System.out.print(x + ",");
			
		}
	}

}
