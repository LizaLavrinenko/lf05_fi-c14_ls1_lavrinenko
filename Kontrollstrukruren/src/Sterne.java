import java.util.Scanner;

public class Sterne {

	public static void main(String[] args) {
		/*
		 Schreiben Sie ein Programm, das eine von Ihnen vorgegebene Anzahl von Sternen (*) in
	Form eines Dreiecks auf dem Bildschirm ausgibt.
	Tipp: Es existiert eine L�sung unter Nutzung einer Schleife und eine weitere L�sung mit zwei
	verschachtelten Schleifen.
		 */
		
	Scanner myscanner = new Scanner(System.in);
	 System.out.print("Wieviel Sterne? ");
	 int Sterne = myscanner.nextInt();
	 int gedruckteSterne = 0;
	 for (int i=1; Sterne>i;i++)
	 {
		 if (Sterne == gedruckteSterne) {
			 break;
		 }
		 for (int y=0;i>y;y++) 
		 {
			 System.out.print("*");
			 gedruckteSterne++;
			 if (Sterne == gedruckteSterne) {
				 break;
			 }
		 } 
		 System.out.print("\n");
	 }

	}

}
