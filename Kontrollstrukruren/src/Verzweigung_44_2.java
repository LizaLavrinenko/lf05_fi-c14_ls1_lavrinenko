import java.util.Scanner;
public class Verzweigung_44_2 {

	public static char jaNein() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Erm��igter Steuersatz (j) oder normaler Steuersatz (n)?");
		
		return scanner.next().charAt(0);
	}
	
	public static void main(String[] args) {
		double steuersatzNormal = 19.0;
		double steuersatzErm��igt = 7.0;
		
		System.out.println("Geben Sie Ihre Nettowerte ein: ");
		
		Scanner scanner = new Scanner(System.in);
		double wert = scanner.nextDouble();
		
		char antwort = jaNein();
		
		if ('j' == antwort) {
			System.out.println("Erm��igter Steuersatz: " + (wert + (wert * steuersatzErm��igt/100)));
		}
		if ('n' == antwort) {
			System.out.println("Normaler Steuersatz: " + (wert + ( wert* steuersatzNormal/100)));
		}

	}

}
