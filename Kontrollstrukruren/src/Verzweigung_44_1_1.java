import java.util.Scanner;

public class Verzweigung_44_1_1 {

	public static double zahlEingeben() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein: ");
		
		return scanner.nextDouble();
	}
	
	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		double zahl3;
		
		zahl1 = zahlEingeben();
		zahl2 = zahlEingeben();
		zahl3 = zahlEingeben();
		
		//1.
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Zahl1 ist gr��er als Zahl2 und Zahl3");
		}
		//2.
		if (zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Zahl3 ist gr��er als Zahl1 oder als Zahl2");
		}
		//3.
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println(zahl1);
		} else if (zahl2 > zahl3) {
			System.out.println(zahl2);
		} else {
			System.out.println(zahl3);
		}
		
	}

}
